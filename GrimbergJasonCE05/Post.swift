//
//  Post.swift
//  GrimbergJasonCE05
//

import Foundation
import UIKit

enum ParsingError: Error {
    case PostParsing
    case Generic
}

//Model Object Representing a Reddit Post
class Post {

    /* Stored Properites */
    let title: String
    let firstName: String
    let lastName: String
    let dateOfBirth: String
    let chamber: String
    let party: String
    let lastUpdate: String
    let id: String


    /* Computed Properties */
    var fullName: String {
        return "\(firstName) \(lastName)"
    }

    /* Initializers */
    init(title: String, firstName: String, lastName: String, dateOfBirth: String, chamber: String, party: String, lastUpdate: String, id: String){
        self.title = title
        self.firstName = firstName
        self.lastName = lastName
        self.dateOfBirth = dateOfBirth
        self.chamber = chamber
        self.party = party
        self.lastUpdate = lastUpdate
        self.id = id
    }
}
