//
//  DetailsViewController.swift
//  GrimbergJasonCE05
//
//  Created by Jason Grimberg on 9/9/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var partyLabel: UILabel!
    @IBOutlet weak var congressImage: UIImageView!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    
    // Set a variable to hold the data being passed
    var post: Post!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Check to see if there is anything in the post
        if post != nil {
            nameLabel.text = "Name: " + post.fullName
            titleLabel.text = "Title: " + post.title
            dobLabel.text = "DOB: " + post.dateOfBirth
            lastUpdateLabel.text = "Last Updated: " + post.lastUpdate
            
            // Set title of view controller
            self.title = post.fullName
            
            // Download the picture of the congress person
            downloadPicture(congressPerson: post.id)
            
            // Check to see what party the current post is at
            switch post.party {
            case "R":
                // Change the background color
                view.backgroundColor = .red
                
                // Change the color of the text to white
                nameLabel.textColor = UIColor.white
                titleLabel.textColor = UIColor.white
                dobLabel.textColor = UIColor.white
                lastUpdateLabel.textColor = UIColor.white
                partyLabel.textColor = UIColor.white
                partyLabel.text = "Party: Republican"
            case "D":
                // Change the background color
                view.backgroundColor = .blue
                
                // Change the color of the text to white
                nameLabel.textColor = UIColor.white
                titleLabel.textColor = UIColor.white
                dobLabel.textColor = UIColor.white
                lastUpdateLabel.textColor = UIColor.white
                partyLabel.textColor = UIColor.white
                partyLabel.text = "Party: Democrat"
            case "I":
                // Change the background color
                view.backgroundColor = .yellow
                
                // Change the color of the text to black
                nameLabel.textColor = UIColor.black
                titleLabel.textColor = UIColor.black
                dobLabel.textColor = UIColor.black
                lastUpdateLabel.textColor = UIColor.black
                partyLabel.textColor = UIColor.black
                partyLabel.text = "Party: Independent"
            default:
                partyLabel.text = "Should not get to this point"
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Download and the parse the data for image
    func downloadPicture(congressPerson: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let picUrl = "https://theunitedstates.io/images/congress/225x275/"
        let extensionUrl = ".jpg"
        
        if let validURL = URL(string: picUrl + congressPerson + extensionUrl) {
            let request = URLRequest(url: validURL)
            
            let task = session.dataTask(with: request, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail Out on error
                if opt_error != nil { return }
                
                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return
                }
                
                let image = UIImage(data: data)
                
                //Do UI Stuff
                DispatchQueue.main.async {
                    self.congressImage.image = image
                }
            })
            // Resume the task
            task.resume()
        }
    }
}
