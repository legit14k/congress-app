//
//  VC_Extension.swift
//  GrimbergJasonCE05
//

import Foundation
import UIKit

extension TableViewController{

    // Download and the parse the data
    func downloadJSON(atURL urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        if let validURL = URL(string: urlString) {
            var request = URLRequest(url: validURL)
            
            // Set the kay and the value of the key for the request
            request.setValue("ho9QqEMHvLlmqok2T2jFP7na9odkKYc5wlhBilp9", forHTTPHeaderField: "X-API-Key")
            
            // Set what type of method this request is
            request.httpMethod = "GET"
            
            let task = session.dataTask(with: request, completionHandler: { (opt_data, opt_response, opt_error) in

                //Bail Out on error
                if opt_error != nil { assertionFailure(); return }

                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { assertionFailure(); return
                }

                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {

                        // Starting with the results array of objects and moving down
                        guard let outerData = json["results"] as? [[String: Any]]
                            else { assertionFailure(); return }
                        
                        // Loop throught the results
                        for inner in outerData {
                            // Get to the members array
                            guard let mem = inner["members"] as? [[String: Any]],
                                let chamber = inner["chamber"] as? String
                            else { assertionFailure(); return }
                            
                            // Then loop through the members and grab the data that is needed
                            for i in mem {
                                // First name, last name, title, dob, and picture.
                                guard let fname = i["first_name"] as? String,
                                let lname = i["last_name"] as? String,
                                let title = i["title"] as? String,
                                let dob = i["date_of_birth"] as? String,
                                let party = i["party"] as? String,
                                let lastUpdate = i["last_updated"] as? String,
                                let id = i["id"] as? String
                                    else { assertionFailure(); return }
                                //Create and Append the current post object
                                let currentPost = (Post(title: title, firstName: fname, lastName: lname, dateOfBirth: dob, chamber: chamber, party: party, lastUpdate: lastUpdate, id: id))
                                self.posts.append(currentPost)
                            }
                        }
                    }
                }
                    // Catch any error
                catch {
                    print(error.localizedDescription)
                    assertionFailure();
                }

                //Do UI Stuff
                DispatchQueue.main.async {
                    self.filterPostsByParty()
                    self.tableView.reloadData()
                }
            })
            // Resume the task
            task.resume()
        }
    }
    
    // Filter through each party
    func filterPostsByParty() {

        filteredPosts[0] = posts.filter({ $0.party == "R" })
        filteredPosts[1] = posts.filter({ $0.party == "D" })
        filteredPosts[2] = posts.filter({ $0.party == "I" })
    }
}
