//
//  TableViewController.swift
//  GrimbergJasonCE05
//
//  Created by Jason Grimberg on 9/8/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    // Data Array for our initial download
    var posts = [Post]()
    
    // Containers to hold posts filtered by subreddit
    var filteredPosts = [[Post](), [Post](), [Post]()]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Download and parse data from house and senate
        downloadJSON(atURL: "https://api.propublica.org/congress/v1/115/senate/members.json")
        downloadJSON(atURL: "https://api.propublica.org/congress/v1/115/house/members.json")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows based on the count of the filtered posts
        return filteredPosts[section].count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? TableViewCell
            else { return tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) }
        
        // Configure the cell
        let currentPost = filteredPosts[indexPath.section][indexPath.row]
        
        // Set all the labels in the cells
        cell.NameLabel.text = "Name: " + currentPost.fullName
        cell.ChamberLabel.text = "Chamber: " + currentPost.chamber
        cell.PartyLabel.text = "Party: " + currentPost.party
        cell.TitleLabel.text = "Title: " + currentPost.title
        
        // Switch through all parties and color all cells
        switch currentPost.party {
        case "D":
            cell.backgroundColor = UIColor.blue
        case "R":
            cell.backgroundColor = UIColor.red
        case "I":
            cell.backgroundColor = UIColor.yellow
        default:
            cell.backgroundColor = UIColor.yellow
        }
        return cell
    }
    
    // MARK: - Header Methods
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Set the header and colors for the current cells
        switch section {
        case 0:
            tableView.backgroundColor = UIColor.clear
            return "Republican"
        case 1:
            tableView.backgroundColor = UIColor.clear
            return "Democrat"
        case 2:
            tableView.backgroundColor = UIColor.clear
            return "Independent"
        default:
            return "Should not happen here"
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let indexPath = tableView.indexPathForSelectedRow {
            let postToSend = filteredPosts[indexPath.section][indexPath.row]
            
            if let destination = segue.destination as? DetailsViewController {
                destination.post = postToSend
            }
        }
    }
}
